/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.test;

import br.com.senac.Principal;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sala304b
 */
public class Teste {

    public Teste() {
    }
    @Test
    public void calcularCustoFinal(){
        Principal calc = new Principal();
        double custo = 10000;
        double result = calc.calculadora(custo);
        
        assertEquals(17300, result, 0.01);
    }
}
