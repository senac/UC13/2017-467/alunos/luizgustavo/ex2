package br.com.senac;



public class Principal {
/*O custo ao consumidor de um carro novo é a soma do custo de fábrica com a
percentagem do distribuidor e dos impostos (aplicados ao custo de fábrica). Supondo
que a percentagem do distribuidor seja de 28% e os impostos de 45%, escreva um
algoritmo que leia o custo de fábrica de um carro e escreva o custo ao consumidor.
*/
    
   
    private final double PORCENTAGEM_DISTRIBUIDOR = 0.28;
    private final double IMPOSTO = 0.45;
    
    public double calculadora(double custoDeFabrica){
        double imposto = custoDeFabrica * IMPOSTO;
        double lucro = custoDeFabrica * PORCENTAGEM_DISTRIBUIDOR;
        double resultado = custoDeFabrica + imposto + lucro;
        
        return resultado;
    }
        
    
    
    
    
    
    
    
    
}
